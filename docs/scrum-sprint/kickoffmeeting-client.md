# Kick Off Meeting Client

Merupakan kegiatan yang dilakukan oleh PIC dalam hal ini Produt Owner untuk 
menyamakan ekpektasi dan persepsi dengan project yang nantinya akan dikerjakan. 

Kegiatan ini tidak wajib untuk dilakukan, namun dari kami sangat menganjurkan hal ini 
dilakukan guna untuk memahami konteks product backlog terlebih dahulu Product 
Owner dan Scrum Master sebelum masuk ke kegiatan Kick Off Meeting Internal
dengan tim.

## Waktu

- 1 hari sebelum pelaksanaan scrum planning meeting

## Bahan

- Product backlog yang akan di lakukan proses development

## Peserta

- Client
- Product Owner
- Scrum Master

## Agenda

- 15 menit : Ramah tamah dengan client
- 1 jam    : Diskusi product backlog antara Client dengan Product Owner
- 1 jam    : Melakukan pemrioritasan product backlog
- 15 menit : Penutup

Berikut ini adalah contoh kegiatan yang bisa dijadikan acuan untuk melakukan kick 
off meeting dengan client

- Overview product backlog yang sudah di susun kepada client
- Susunan job story, acceptance criteria
- Menceritakan kemungkinan akan adanya bug pada saat setelah sprint selesai 
  dilaksanakan dan memberikan informasi keterkaitan ticketing dan acceptance 
  criteria dalam sprint
- Menceritakan kemungkinan job story yang nantinya bisa di ambil dalam satu 
  sprint berdasarkan dengan rata-rata jumlah product backlog yang bisa di ambil 
  sekitar 6 - 9 job story
- Melakukan diskusi dengan client untuk melakukan prioritas job story yang harus di selesaikan 

## Tujuan

- Memberikan pemahaman product backlog
- Mendapatkan prioritas job story yang nantinya akan dikerjakan dalam satu sprint 

## Artefak

- [ ] Notulen rapat hasil kick off meeting dengan client

## Pelaporan

Scrum Master melakukan :

- pencatan hasil meeting dengan format yang terdapat pada link 
  https://docs.google.com/document/d/11693Gh_Aou92ifVJJrda0p34v9n1d5UdaRM9-iX3rkM/edit?usp=sharing
- bersama dengan product owner menandatangani notulen rapat tersebut
- mengirimkan hasil rapat ke email client dengan tembisan `production@labtekindie.com`
- notulen rapat harus disampaikan paling telat 1 (satu) setelah kegiatan tersebut 
  selesai dilakukan
