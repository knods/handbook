# Interaksi Sebagai Assitant Researcher

Dokumen ini bertujuan untuk memberikan panduan kerja seorang assistant researcher di tahapan kerja in-depth interview atau usability testing. Silakan gunakan panduan ini untuk memahami apa saja yang perlu dilakukan seorang assistant researcher.

## Sebelum Kegiatan

Sebelum kegiatan in-depth interview atau usability testing dimulai, seorang assistant researcher harus melakukan kick-off dengan lead researcher. Tujuan dari kegiatan ini adalah untuk memahami tujuan kegiatan, detail pelaksanaan kegiatan (kapan akan dimulai, kapan report harus dihasilkan, jumlah dan persona responden, serta hal lainnya), serta poin-poin penting apa saja yang harus diketahui seorang assistan resercher. 

Setelah kick off, assistant researcher bisa melakukan brainstorming dengan lead researcher untuk membahas detail kegiatan, seperti pertanyaan apa saja yang harus ditanyakan saat wawancara, skenario testing untuk usability testing, pertanyaan apa saja yang harus ditanyakan untuk filtering awal responden, dan seterusnya. Lead researcher bertanggung jawab untuk menghasilkan detail kegiatan ini, namun assitant researcher perlu membantu lead, sehingga saat pelaksanaan, semua pihak bisa memahami apa saja yang harus dilakukan.

## Selama Kegiatan

Selama kegiatan in-depth interview atau usability testing berlangsung, assitant researcher akan mendampingi lead researcher untuk berinteraksi dengan para responden. Selama wawancara atau testing berlangsung, assitant researcher bertanggung jawab memastikan kegiatan bisa terdokumentasi (ada rekaman audio/video dan atau foto). Poin-poin penting yang ditemukan selama wawancara juga diperkenankan untuk ditulis, karena lead researcher mungkin akan fokus pada melakukan wawancara, sehingga belum tentu bisa mencatat temuan. Jika diperkenankan oleh lead researcher, seorang assistant bisa juga berinteraksi dengan responden.

## Usai Kegiatan

Setelah 1 kegiatan berlangsung (1 wawancara atau 1 pengujian), assitant researcher berkewajiban untuk melakukan 2 hal:

1. Mengupload rekaman video ke akun YouTube Labtek Indie (akses akan diberikan oleh tim QA/QC Labtek Indie)
2. Mendownload hasil wawancara dalam format UX nuggets di base Airtable project (akses juga akan diberikan oleh tim QA/QC Labtek Indie)

Untuk menjaga momentum dan memastikan tidak ada data tercecer, kedua hal tersebut sebaiknya dilakukan setelah setiap sesi wawancara/testing. Pengisian UX nuggets bisa dilakukan mengikuti format data yang sudah ada di Airtable, namun jika ada kesulitan, assistant researcher bisa berdiskusi dengan lead researcher atau QA/QC Labtek Indie.

Saat akan membuat laporan akhir, seorang lead researcher perlu memastikan semua data sudah tercatat dengan benar di Airtable dalam format UX nuggets. Jika ditemukan ada yang perlu diperbaiki dalam rangka analisa data, maka lead researcher diperkenankan untuk berdiskusi dengan assistant researcher, sehingga laporan akhir bisa dibuat dengan baik.