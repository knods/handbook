# Interaksi Sebagai Co-facilitator

Dokumen ini bertujuan untuk memberikan panduan kerja seorang co-facilitator di tahapan kerja co-creation. Silakan gunakan panduan ini untuk memahami apa saja yang perlu dilakukan seorang co-facilitator.

## Sebelum Kegiatan

Sebelum kegiatan co-creation dimulai, seorang co-facilitator harus melakukan kick-off dengan lead facilitator. Tujuan dari kegiatan ini adalah untuk memahami tujuan kegiatan, rundown acara, serta poin-poin penting apa saja yang harus diketahui seorang co-facilitator. Nantinya co-facilitator akan mendampingi setiap kelompok di sesi co-creation, sehingga penting bagi mereka untuk mengenali sedari awal, siapa saja yang akan berinteraksi erat dengan dirinya.

## Selama Kegiatan

Selama kegiatan co-creation berlangsung, seorang co-facilitator akan mendampingin kelompok kecil (yang merupakan bagian dari kelompok besar peserta keseluruhan) untuk melakukan rangkaian kegiatan yang diarahkan oleh lead facilitator. Ini termasuk memfasilitasi diskusi dalam kelompok, menjawab kebingungan yang mungkin dialami peserta, hingga memastikan diskusi bisa berjalan lancar untuk mencapai hasil yang diharapkan.

## Usai Kegiatan

Setelah kegiatan berlangsung, co-facilitator memiliki kewajiban untuk mendokumentasikan semua artifak kegiatan yang dihasilkan kelompoknya. Ini termasuk merapikan tulisan di Miro atau papan tulis, memfoto artifak (jika kegiatan offline), termasuk menambahkan anotasi di dalam artifak yang mungkin membantu lead facilitator dalam membuat laporan akhir kegiatan.

Tidak kalah penting adalah mengikuti retrospective meeting dengan lead-facilitator, untuk mereview kegiatan dan melihat apa yang bisa diperbaiki di kegiatan serupa di masa depan.